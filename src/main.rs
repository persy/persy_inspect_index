use clap::Parser;
use persy::OpenOptions;
use persy_inspect_index::{print_index, Ops};
use std::{error::Error, result::Result};

#[derive(Parser)]
struct Args {
    file: String,
    index: String,
    /// Print the tree in a compact way
    #[arg(default_value_t = false, short, long)]
    compact: bool,
    /// Print the index
    #[arg(default_value_t = true, short, long)]
    print: bool,
    /// Check the index
    #[arg(default_value_t = true, short, long)]
    check: bool,
}

impl From<&Args> for Ops {
    fn from(value: &Args) -> Self {
        Self {
            compact: value.compact,
            print: value.print,
            check: value.check,
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let pars = Args::parse();
    let ops = Ops::from(&pars);
    let persy = OpenOptions::new().open(pars.file)?;
    for (name, info) in persy.list_indexes()? {
        if name == pars.index {
            print_index(persy.clone(), &name, &info, &ops)?;
        }
    }
    Ok(())
}
