use persy::{
    inspect::{PersyInspect, PrintTreeInspector, TreeInspector, TreeInspectorResult},
    ByteVec, IndexInfo, IndexType, IndexTypeId, Persy, PersyId,
};
use std::{error::Error, fmt::Display, result::Result};

pub struct Ops {
    /// Print the tree in a compact way
    pub compact: bool,
    /// Print the index
    pub print: bool,
    /// Check the index
    pub check: bool,
}

struct StackNode<K> {
    count: u32,
    current_next: Option<K>,
    current_key: Option<K>,
}
impl<K> StackNode<K> {
    fn new(current_next: Option<K>) -> Self {
        Self {
            count: 0,
            current_next,
            current_key: None,
        }
    }
    fn inc(&mut self, current_key: K) {
        self.current_key = Some(current_key);
        self.count += 1;
    }
    fn count(&self) -> u32 {
        self.count
    }
    fn current_next(&self) -> &Option<K> {
        &self.current_next
    }

    fn current_key(&self) -> &Option<K> {
        &self.current_key
    }
}
struct TraverseStack<K> {
    stack: Vec<StackNode<K>>,
}
impl<K> Default for TraverseStack<K> {
    fn default() -> Self {
        Self { stack: Vec::new() }
    }
}
impl<K> TraverseStack<K> {
    fn start_node(&mut self, current_next: Option<K>) {
        self.stack.push(StackNode::new(current_next));
    }
    fn hit_key(&mut self, current_key: K) {
        self.stack.last_mut().unwrap().inc(current_key);
    }
    fn end_node(&mut self, res: &Box<dyn CheckResult<K>>, id: &PersyId) {
        let val = self.stack.pop().unwrap();
        let level = self.stack.len();
        if val.count() < 30 && level != 0 {
            res.too_few_elements(id, val.count(), level);
        } else if val.count() > 140 {
            res.too_few_elements(id, val.count(), level);
        }
    }

    fn last_next(&self) -> &Option<K> {
        if let Some(e) = self.stack.last() {
            e.current_next()
        } else {
            &None
        }
    }

    fn last_key(&self) -> &Option<K> {
        if let Some(e) = self.stack.last() {
            e.current_key()
        } else {
            &None
        }
    }

    fn last_count(&self) -> u32 {
        if let Some(e) = self.stack.last() {
            e.count()
        } else {
            0
        }
    }
}

pub trait CheckResult<K> {
    fn prev_next_not_match(&self, prev: &K, next: &K, id: &PersyId);
    fn parent_key_dont_match_prev(&self, prev: &Option<K>, parent: &Option<K>);
    fn last_key_dont_match_prev(&self, prev: &Option<K>, parent: &Option<K>);
    fn missing_prev(&self, id: &PersyId);
    fn fail_load(&self, id: &PersyId);
    fn too_few_elements(&self, id: &PersyId, size: u32, level: usize);
    fn too_many_elements(&self, id: &PersyId, size: u32, level: usize);
}
pub struct PrintCheckResult();
impl<K: Display> CheckResult<K> for PrintCheckResult {
    fn prev_next_not_match(&self, prev: &K, prev_next: &K, id: &PersyId) {
        println!("prev and next for {} not the same  {} {}", id, prev, prev_next);
    }

    fn parent_key_dont_match_prev(&self, prev: &Option<K>, parent: &Option<K>) {
        println!(
            "parent key:{} do not match prev: {}",
            parent.as_ref().map(|x| format!("{}", x)).unwrap_or("None".to_owned()),
            prev.as_ref().map(|x| format!("{}", x)).unwrap_or("None".to_owned())
        );
    }

    fn last_key_dont_match_prev(&self, prev: &Option<K>, last_key: &Option<K>) {
        println!(
            "not matchin key {:?} and prev {:?}",
            last_key.as_ref().map(|x| format!("{}", x)),
            prev.as_ref().map(|x| format!("{}", x))
        );
    }

    fn missing_prev(&self, id: &PersyId) {
        println!("{} expected to have a prev but it does not", id);
    }
    fn fail_load(&self, id: &PersyId) {
        println!("fail load {}", id);
    }

    fn too_few_elements(&self, id: &PersyId, size: u32, level: usize) {
        println!("found too few elements {} at level {} for {}", size, level, id);
    }

    fn too_many_elements(&self, id: &PersyId, size: u32, level: usize) {
        println!("found too many elements {} at level {} for {}", size, level, id);
    }
}

pub struct FailCheckResults();
impl<K: Display> CheckResult<K> for FailCheckResults {
    fn prev_next_not_match(&self, prev: &K, prev_next: &K, id: &PersyId) {
        panic!("prev and next for {} not the same  {} {}", id, prev, prev_next);
    }

    fn parent_key_dont_match_prev(&self, prev: &Option<K>, parent: &Option<K>) {
        panic!(
            "parent key:{} do not match prev: {}",
            parent.as_ref().map(|x| format!("{}", x)).unwrap_or("None".to_owned()),
            prev.as_ref().map(|x| format!("{}", x)).unwrap_or("None".to_owned())
        );
    }

    fn last_key_dont_match_prev(&self, prev: &Option<K>, last_key: &Option<K>) {
        panic!(
            "not matchin key {:?} and prev {:?}",
            last_key.as_ref().map(|x| format!("{}", x)),
            prev.as_ref().map(|x| format!("{}", x))
        );
    }

    fn missing_prev(&self, id: &PersyId) {
        panic!("{} expected to have a prev but it does not", id);
    }
    fn fail_load(&self, id: &PersyId) {
        panic!("fail load {}", id);
    }

    fn too_few_elements(&self, id: &PersyId, size: u32, level: usize) {
        panic!("found too few elements {} at level {} for {}", size, level, id);
    }

    fn too_many_elements(&self, id: &PersyId, size: u32, level: usize) {
        panic!("found too many elements {} at level {} for {}", size, level, id);
    }
}

pub struct CheckPrintTreeInspector<K> {
    next_check: Option<K>,
    stack: TraverseStack<K>,
    res: Box<dyn CheckResult<K>>,
}

impl<K: Eq + Clone> CheckPrintTreeInspector<K> {
    pub fn new(res: Box<dyn CheckResult<K>>) -> Self {
        Self {
            next_check: Default::default(),
            stack: TraverseStack::default(),
            res,
        }
    }
    fn start_check_prev_next(&mut self, prev: Option<K>, next: Option<K>, id: &PersyId) {
        //Check if next & prev across nodes are the same
        if let Some(pv) = &self.next_check {
            if let Some(p) = &prev {
                if p != pv {
                    self.res.prev_next_not_match(p, pv, id);
                }
            } else {
                self.res.missing_prev(id);
            }
        }
        // check if prev and parent key are the same, which imply is also equal to the previous
        // next
        if &prev != self.stack.last_key() && self.stack.last_count() != 0 {
            self.res.parent_key_dont_match_prev(&prev, self.stack.last_key());
        }

        self.stack.start_node(next);
    }
    fn end_check_counts(&mut self, id: &PersyId) {
        let val = self.stack.last_next();
        self.next_check = val.clone();
        self.stack.end_node(&self.res, id)
    }
}

impl<'a, K, V> TreeInspector<K, V> for CheckPrintTreeInspector<K>
where
    K: Display + Eq + Clone,
    V: Display,
{
    fn start_node(&mut self, node_id: PersyId, prev: Option<K>, next: Option<K>) -> TreeInspectorResult<()> {
        self.start_check_prev_next(prev, next, &node_id);
        Ok(())
    }

    fn end_node(&mut self, node_id: PersyId) -> TreeInspectorResult<()> {
        self.end_check_counts(&node_id);
        Ok(())
    }

    fn start_leaf(&mut self, node_id: PersyId, prev: Option<K>, next: Option<K>) -> TreeInspectorResult<()> {
        self.start_check_prev_next(prev, next, &node_id);
        Ok(())
    }

    fn end_leaf(&mut self, node_id: PersyId) -> TreeInspectorResult<()> {
        self.end_check_counts(&node_id);
        Ok(())
    }

    fn start_key(&mut self, _node_pos: u32, k: K) -> TreeInspectorResult<()> {
        self.stack.hit_key(k);
        Ok(())
    }

    fn failed_load(&mut self, node_id: PersyId) -> TreeInspectorResult<()> {
        self.res.fail_load(&node_id);
        Ok(())
    }

    fn empty(&mut self) -> TreeInspectorResult<()> {
        Ok(())
    }
}

fn print_index_k_v<K, V>(persy: Persy, name: &str, ops: &Ops) -> Result<(), Box<dyn Error>>
where
    K: IndexType + Display + Eq,
    V: IndexType + Display,
{
    if ops.print {
        println!("start print");
        let mut inspector = if ops.compact {
            PrintTreeInspector::new()
        } else {
            PrintTreeInspector::new_print_leaf()
        };
        persy.inspect_tree::<K, V, _>(name, &mut inspector)?;
        println!("end print");
    }
    if ops.check {
        println!("start check");
        persy.inspect_tree::<K, V, _>(
            name,
            &mut CheckPrintTreeInspector::<K>::new(Box::new(PrintCheckResult())),
        )?;
        println!("end check");
    }
    Ok(())
}
fn print_index_k<K>(persy: Persy, name: &str, index_info: &IndexInfo, ops: &Ops) -> Result<(), Box<dyn Error>>
where
    K: IndexType + Display + Eq,
{
    match index_info.value_type {
        IndexTypeId::U8 => print_index_k_v::<K, u8>(persy, name, ops)?,
        IndexTypeId::U16 => print_index_k_v::<K, u16>(persy, name, ops)?,
        IndexTypeId::U32 => print_index_k_v::<K, u32>(persy, name, ops)?,
        IndexTypeId::U64 => print_index_k_v::<K, u64>(persy, name, ops)?,
        IndexTypeId::U128 => print_index_k_v::<K, u128>(persy, name, ops)?,
        IndexTypeId::I8 => print_index_k_v::<K, i8>(persy, name, ops)?,
        IndexTypeId::I16 => print_index_k_v::<K, i16>(persy, name, ops)?,
        IndexTypeId::I32 => print_index_k_v::<K, i32>(persy, name, ops)?,
        IndexTypeId::I64 => print_index_k_v::<K, i64>(persy, name, ops)?,
        IndexTypeId::I128 => print_index_k_v::<K, i128>(persy, name, ops)?,
        IndexTypeId::F32W => print_index_k_v::<K, f32>(persy, name, ops)?,
        IndexTypeId::F64W => print_index_k_v::<K, f64>(persy, name, ops)?,
        IndexTypeId::String => print_index_k_v::<K, String>(persy, name, ops)?,
        IndexTypeId::PersyId => print_index_k_v::<K, PersyId>(persy, name, ops)?,
        IndexTypeId::ByteVec => print_index_k_v::<K, ByteVec>(persy, name, ops)?,
    }
    Ok(())
}
pub fn print_index(persy: Persy, name: &str, index_info: &IndexInfo, ops: &Ops) -> Result<(), Box<dyn Error>> {
    match index_info.key_type {
        IndexTypeId::U8 => print_index_k::<u8>(persy, name, index_info, ops)?,
        IndexTypeId::U16 => print_index_k::<u16>(persy, name, index_info, ops)?,
        IndexTypeId::U32 => print_index_k::<u32>(persy, name, index_info, ops)?,
        IndexTypeId::U64 => print_index_k::<u64>(persy, name, index_info, ops)?,
        IndexTypeId::U128 => print_index_k::<u128>(persy, name, index_info, ops)?,
        IndexTypeId::I8 => print_index_k::<i8>(persy, name, index_info, ops)?,
        IndexTypeId::I16 => print_index_k::<i16>(persy, name, index_info, ops)?,
        IndexTypeId::I32 => print_index_k::<i32>(persy, name, index_info, ops)?,
        IndexTypeId::I64 => print_index_k::<i64>(persy, name, index_info, ops)?,
        IndexTypeId::I128 => print_index_k::<i128>(persy, name, index_info, ops)?,
        IndexTypeId::F32W => todo!(), //print_index_k::<f32>(persy, name, index_info, compact)?,
        IndexTypeId::F64W => todo!(), //print_index_k::<f64>(persy, name, index_info, compact)?,
        IndexTypeId::String => print_index_k::<String>(persy, name, index_info, ops)?,
        IndexTypeId::PersyId => print_index_k::<PersyId>(persy, name, index_info, ops)?,
        IndexTypeId::ByteVec => print_index_k::<ByteVec>(persy, name, index_info, ops)?,
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::{CheckPrintTreeInspector, FailCheckResults};
    use persy::{inspect::PersyInspect, OpenOptions, ValueMode};
    #[test]
    fn simple_checker_test() {
        let persy = OpenOptions::new().memory().expect("can create memory db");

        let mut tx = persy.begin().expect("begin transaction works");
        let index_name = "simple_inspect";
        tx.create_index::<i32, i32>(index_name, ValueMode::Cluster).unwrap();
        let prep = tx.prepare().expect("prepare with index works");
        prep.commit().expect("commit with index works");
        let mut tx = persy.begin().expect("begin transaction works");
        for v in 0..30000 {
            tx.put::<i32, i32>(index_name, v, 12).expect("put works correctly");
            tx.put::<i32, i32>(index_name, v, 13).expect("put works correctly");
            tx.put::<i32, i32>(index_name, v, 14).expect("put works correctly");
            if v % 100 == 0 {
                let prep = tx.prepare().expect("prepare with index works");
                prep.commit().expect("commit with index works");
                tx = persy.begin().expect("begin transaction works");
            }
        }
        let mut inspector = CheckPrintTreeInspector::new(Box::new(FailCheckResults()));
        persy
            .inspect_tree::<i32, i32, _>(index_name, &mut inspector)
            .expect("inspect works");
    }
}
